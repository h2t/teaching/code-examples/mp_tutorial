global testTraj starts goals tau paras Ws phaseStop

testparas = paras;
testparas.dt = 0.001;

%% set test parameters
alpha_phaseStop = phaseStop;
testparas.y0 = starts;
testparas.dy = [0,0];
testparas.goals = goals;
testparas.tau = tau;
testparas.ac = phaseStop;


testparas.extForce = [0,0,0,0];



testTraj = dmptest(Ws, testparas);
